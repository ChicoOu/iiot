import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-sensor',
    templateUrl: './sensor.component.html',
    styleUrls: ['./sensor.component.css']
})

export class SensorComponent implements OnInit, OnDestroy {
    data: number[] = [];
    now: Date = new Date(2010, 10, 20);
    oneDay: number = 24 * 3600 * 1000;
    value: number = Math.random() * 1000;
    timer: any;

    options: any = {
        title: {
            text: '传感器数据监控'
        },
        tooltip: {
            trigger: 'axis',
            formatter: function (params) {
                params = params[0];
                const date: Date = new Date(params.name);
                return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + ' : ' + params.value[1];
            },
            axisPointer: {
                animation: false
            }
        },
        xAxis: {
            type: 'time',
            splitLine: {
                show: false
            }
        },
        yAxis: {
            type: 'value',
            boundaryGap: [0, '100%'],
            splitLine: {
                show: false
            }
        },
        series: [{
            name: '模拟数据',
            type: 'line',
            showSymbol: false,
            hoverAnimation: false,
            data: this.data
        }]
    };

    constructor() {
        for (let i = 0; i < 1000; i++) {
            this.data.push(this.randomData());
        }
    }

    ngOnInit(): void {
        this.timer = setInterval(() => {
            for (let i = 0; i < 5; i++) {
                this.data.shift();
                this.data.push(this.randomData());
            }

            this.options.series[0].data = this.data;
            /*myChart.setOption({
                series: [{
                    data: data
                }]
            });*/
        }, 1000);
    }

    ngOnDestroy(): void {
        clearInterval(this.timer);
    }

    randomData(): any {
        this.now = new Date(+this.now + this.oneDay);
        this.value = this.value + Math.random() * 21 - 10;
        return {
            name: this.now.toString(),
            value: [
                [this.now.getFullYear(), this.now.getMonth() + 1, this.now.getDate()].join('/'),
                Math.round(this.value)
            ]
        };
    }
}
