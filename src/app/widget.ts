export const enum WidgetCategory{
    JOB = 1,
    DOC = 2,
    UTIL = 3,
    COMM = 4,
    SETTINGS = 5
};

export class Widget{
    imgPath : string;
    title : string;
    category : WidgetCategory;
    isFavorite : boolean;
    name : string
}
