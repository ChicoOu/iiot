import { Component, OnInit, Input } from '@angular/core';
import { Widget } from './widget';
import { WidgetService } from './widget.service'

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css'],
  providers: [WidgetService]
})
export class FavoriteComponent implements OnInit{
  items : Widget[];

  ngOnInit() : void {
        this.getFavoriteWidgets();
  }

  constructor(private widgetService : WidgetService) {
    
  }

  getFavoriteWidgets() {
    this.widgetService.getFavoriteWidgets().then(items => this.items = items);
  }
}
