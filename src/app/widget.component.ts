import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Widget, WidgetCategory } from './widget';
import { WidgetService } from './widget.service';

@Component({
    selector: 'app-widget',
    templateUrl: './widget.component.html',
    styleUrls: ['./widget.component.css']
})

export class WidgetComponent implements OnInit {
    @Input() itemModel: Widget;
    @Input() isMainPanel = true;
    borderColorClass: any;
    baritemClass: any;

    constructor(private router: Router, private widgetService: WidgetService) {

    }

    ngOnInit(): void {
        if (this.itemModel) {
            this.borderColorClass = {
                'green-border': this.itemModel.category == WidgetCategory.JOB,
                'yellow-border': this.itemModel.category == WidgetCategory.UTIL,
                'blue-border': this.itemModel.category == WidgetCategory.DOC,
                'purple-border': this.itemModel.category == WidgetCategory.COMM,
                'orange-border': this.itemModel.category == WidgetCategory.SETTINGS
            };

            this.baritemClass = {
                'main': this.isMainPanel,
                'favorite': !this.isMainPanel
            }
        }
    }

    onClick(): void {
        this.widgetService.widget = this.itemModel;
        this.router.navigate(['/detail', this.itemModel.name]);
    }
}
