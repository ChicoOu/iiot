import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MainPanelComponent } from './mainpanel.component';
import { HeaderComponent } from './header.component';
import { CaptionComponent } from './caption.component';
import { FavoriteComponent } from './favorite.component';
import { WidgetComponent } from './widget.component';
import { JobManagerComponent } from './jobmanager.component';
import { AppRoutingModule } from './app-routing.module';
import { SensorComponent } from './sensor.component';
import { echartsDirective } from './ts-charts.directive';

@NgModule({
    declarations: [
        AppComponent,
        MainPanelComponent,
        HeaderComponent,
        CaptionComponent,
        FavoriteComponent,
        WidgetComponent,
        JobManagerComponent,
        SensorComponent,
        echartsDirective
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
