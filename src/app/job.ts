export const enum JobPriority {
    A, B, C, D
};

export class Job {
    imgPath: string;
    name: string;
    jobId: string;
    startDate: Date;
    endDate: Date;
    client: string;
    exeDate: Date;
    priority: JobPriority;
    useable: number;
    needed: number;
    finished: number;
    failed: number;
    material: string;
    files: string[];
}
