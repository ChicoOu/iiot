import { Component, Input, OnInit } from '@angular/core';
import { Job, JobPriority } from './job';
import { JobService } from './job.service';

@Component({
    selector: 'app-jobnav',
    templateUrl: './job-nav.component.html',
    styleUrls: ['./job-nav.component.css']
})

export class JobNavComponent implements OnInit {
    @Input() itemModel: Job;

    constructor(private jobService: JobService) {

    }

    ngOnInit(): void {

    }
}
