import { Component, OnInit } from '@angular/core';
import { WidgetService } from './widget.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [WidgetService]
})
export class AppComponent {
    constructor(private widgetService: WidgetService) {

    }
}
