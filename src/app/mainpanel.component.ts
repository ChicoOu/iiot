import { Component, Input, OnInit } from '@angular/core';
import { Widget, WidgetCategory } from './widget';
import { WidgetService } from './widget.service';

@Component({
    selector: 'app-mainpanel',
    templateUrl: './mainpanel.component.html',
    styleUrls: ['./mainpanel.component.css']
})

export class MainPanelComponent implements OnInit {
    items: Widget[];

    constructor(private widgetService: WidgetService) {

    }

    getWidgets(): void {
        this.widgetService.getWidgets().then(items => this.items = items);
    }

    ngOnInit(): void {
        this.getWidgets();
    }
}
