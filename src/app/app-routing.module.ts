import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainPanelComponent } from './mainpanel.component';
import { JobManagerComponent } from './jobmanager.component';
import { SensorComponent } from './sensor.component';

const routes: Routes = [
    { path: '', redirectTo: '/mainpanel', pathMatch: 'full' },
    { path: 'mainpanel', component: MainPanelComponent },
    { path: 'detail/jobmanager', component: JobManagerComponent },
    { path: 'detail/monitor', component: SensorComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }