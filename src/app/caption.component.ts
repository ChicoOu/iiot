import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Widget } from './widget';
import { WidgetService } from './widget.service';

@Component({
    selector: 'app-caption',
    templateUrl: './caption.component.html',
    styleUrls: ['./caption.component.css']
})
export class CaptionComponent implements OnInit, OnDestroy {
    @Input() title: string;
    @Input() name: string;
    desc: string;
    intervalId: any;

    ngOnInit(): void {
        this.start();
    }

    ngOnDestroy(): void {
        this.stop();
    }

    constructor(private router: Router, private widgetService: WidgetService) {

    }

    clearTimer() {
        clearInterval(this.intervalId);
    }

    start() {
        this.clearTimer();
        this.intervalId = setInterval(() => {
            this.desc = this.name + '\t' + new Date().toLocaleTimeString();
        }, 1000);
    }

    stop() {
        this.clearTimer();
    }

    goHome() {
        this.widgetService.setCurWidget(null);
        this.router.navigate(['/mainpanel']);
    }
}
