import { Injectable, Input } from '@angular/core';

import { JOBS } from './mock-datasource';
import { Job } from './job';

@Injectable()
export class JobService {
    getJobs(): Promise<Job[]> {
        return Promise.resolve(JOBS);
    }
}