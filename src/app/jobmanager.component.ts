import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { JobService } from './job.service';
import { Job, JobPriority } from './job';

@Component({
    selector: 'app-jobmanager',
    templateUrl: './jobmanager.component.html',
    styleUrls: ['./jobmanager.component.css'],
    providers: [JobService]
})
export class JobManagerComponent implements OnInit {
    jobs: Job[];
    curJob: Job;
    jobForm: FormGroup;
    name: FormControl;
    jobId: FormControl;
    startDate: FormControl;
    endDate: FormControl;
    client: FormControl;
    clientId: FormControl;
    exeDate: FormControl;
    exeTime: FormControl;
    custom: FormControl;
    custom2: FormControl;
    priority: FormControl;
    period: FormControl;

    constructor(private jobService: JobService) {

    }

    createFormControl(): void {
        this.name = new FormControl('',
            [Validators.required,
            Validators.minLength(5),
            Validators.maxLength(8)]);
        this.jobId = new FormControl('',
            [Validators.required, Validators.min(0)]);
        this.startDate = new FormControl('',
            [Validators.required]);
        this.endDate = new FormControl('',
            [Validators.required]);
        this.client = new FormControl('');
        this.clientId = new FormControl('');
        this.exeDate = new FormControl('',
            [Validators.required]);
        this.exeTime = new FormControl('',
            [Validators.required]);
        this.custom = new FormControl('');
        this.custom2 = new FormControl('');
        this.priority = new FormControl('',
            [Validators.required]);
        this.period = new FormControl('');
    }

    createFormGroup(): void {
        this.jobForm = new FormGroup({
            name: this.name,
            jobId: this.jobId,
            startDate: this.startDate,
            endDate: this.endDate,
            client: this.client,
            clientId: this.clientId,
            exeDate: this.exeDate,
            exeTime: this.exeTime,
            custom: this.custom,
            custom2: this.custom2,
            priority: this.priority,
            period: this.period
        });
    }

    ngOnInit(): void {
        this.createFormControl();
        this.createFormGroup();
        this.getJobs();
    }

    getJobPriorityStr(priority: JobPriority): string {
        let result = 'D';
        switch (priority) {
            case JobPriority.A:
                result = 'A';
                break;
            case JobPriority.B:
                result = 'B';
                break;
            case JobPriority.C:
                result = 'C';
                break;
            default:
                break;
        }

        return result;
    }

    parseJobPriorityStr(priority: string): JobPriority {
        let result = JobPriority.D;
        switch (priority) {
            case 'A':
                result = JobPriority.A;
                break;
            case 'B':
                result = JobPriority.B;
                break;
            case 'C':
                result = JobPriority.C;
                break;
            default:
                break;
        }

        return result;
    }

    getJobs(): void {
        this.jobService.getJobs().then(jobs => {
            this.jobs = jobs;
            if (this.jobs != null) {
                this.curJob = this.jobs[0];
            }
        });
    }

    onNavItemClick(job) {
        this.curJob = job;
    }

    parseDate(dateString: string): Date {
        if (dateString) {
            return new Date(dateString);
        } else {
            return null;
        }
    }
}
