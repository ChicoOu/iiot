import { Injectable, Input } from '@angular/core';

import { ITEMS } from './mock-datasource';
import { Widget } from './widget';

@Injectable()
export class WidgetService {
    widget: Widget = null;

    getWidgets(): Promise<Widget[]> {
        return Promise.resolve(ITEMS);
    }

    getFavoriteWidgets(): Promise<Widget[]> {
        let favorites: Widget[] = [];
        ITEMS.forEach(item => {
            if (item.isFavorite) {
                favorites.push(item);
            }
        });

        return Promise.resolve(favorites);
    }

    setCurWidget(cur: Widget) {
        this.widget = cur;
    }

    getCurWidget() {
        return this.widget;
    }
}