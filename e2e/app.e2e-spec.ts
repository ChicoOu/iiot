import { IiotPage } from './app.po';

describe('iiot App', () => {
  let page: IiotPage;

  beforeEach(() => {
    page = new IiotPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
